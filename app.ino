Servo Uplift;  // create servo object to control a servo
Servo Recharge;  // create servo object to control a servo
Servo Energise;  // create servo object to control a servo
Servo Invigorate;  // create servo object to control a servo


bool looking = false;
unsigned long stateTimeStart = 0;

unsigned long timeoutStart = 0;
int timeoutTime = 20000; //after 20 secs, go to idle

//For each assembly, different time
int assemblyTimeShort = 4000;
int assemblyTimeLong = 7000;

enum State {
  IDLE,
  STATE_A,
  STATE_B,
  STATE_C,
  STATE_D,
  STATE_E,
  STATE_F,
  STATE_G,
  STATE_H
} currentState;

int pos = 0;    // variable to store the servo position
State state = IDLE;

void loopState(int assemblyTimeState, State state){
  if (millis() - stateTimeStart < assemblyTimeState){
    assemblyMove();
  }else{
    if (!looking ){
      if (state == STATE_A){
        lookUplift();
      }else if (state == STATE_B){
        lookRecharge();
      }else if (state == STATE_C){
        lookUplift();
      }else if (state == STATE_D){
        lookInvigorate();


      }else if (state == STATE_E){
        lookUplift();
      }else if (state == STATE_F){
        lookEnergise();
      }else if (state == STATE_G){
        lookRecharge();
      }else if (state == STATE_H){
        lookInvigorate();
      }

      looking = true;
    }
  }
}

void finalLoopState(int assemblyTimeState, State state){
  if (millis() - stateTimeStart < assemblyTimeState){
    assemblyMove();
  }else if (millis() - stateTimeStart < 2*assemblyTimeState){
    if (!looking ){
      if (state == STATE_E){
        lookUplift();
      }else if (state == STATE_F){
        lookEnergise();
      }else if (state == STATE_G){
        lookRecharge();
      }else if (state == STATE_H){
        lookInvigorate();
      }

      looking = true;
    }
  }else{
    if (state == STATE_E){
      speakerMove(&Uplift);
    }else if (state == STATE_F){
      speakerMove(&Energise);
    }else if (state == STATE_G){
      speakerMove(&Recharge);
    }else if (state == STATE_H){
      speakerMove(&Invigorate);
    }
  }
}



void setup(){

  Uplift.attach(0);  // attaches the servo on pin 9 to the servo object
  Recharge.attach(1);
  Energise.attach(2);
  Invigorate.attach(3);

  Serial.begin(9600);

  /*
  POST https://api.particle.io/v1/devices/2e0029000947343432313031/assembly

    -d access_token=8037152bd2d7079270fde09e1a3eedd71ba5abdd


  */
  Particle.function("state_a", startStateA);
  Particle.function("state_b", startStateB);
  Particle.function("state_c", startStateC);
  Particle.function("state_d", startStateD);
  Particle.function("state_e", startStateE);
  Particle.function("state_f", startStateF);
  Particle.function("state_g", startStateG);
  Particle.function("state_h", startStateH);
  Particle.function("idle", idle);


}

void loop(){

  if (currentState == IDLE){

  }else if (currentState == STATE_A){

    loopState(assemblyTimeShort, STATE_A);

  }else if (currentState == STATE_B){
    loopState(assemblyTimeShort, STATE_B);
  }else if (currentState == STATE_C){
    loopState(assemblyTimeLong, STATE_C);
  }else if (currentState == STATE_D){
    loopState(assemblyTimeLong, STATE_D);


  // last states - choose bottle
  }else if (currentState == STATE_E){
    finalLoopState(assemblyTimeLong, STATE_E);
  }else if (currentState == STATE_F){
    finalLoopState(assemblyTimeLong, STATE_F);
  }else if (currentState == STATE_G){
    finalLoopState(assemblyTimeLong, STATE_G);
  }else if (currentState == STATE_H){
    finalLoopState(assemblyTimeLong, STATE_H);
  }

  if (currentState != IDLE){
      if (millis() - timeoutStart > timeoutTime) {
        currentState = IDLE;
        faceForward();
      }
  }
}







void faceForward(){

  Uplift.write(110);              // tell servo to go to position in variable 'pos'
  Recharge.write(110);              // tell servo to go to position in variable 'pos'
  Energise.write(110);              // tell servo to go to position in variable 'pos'
  Invigorate.write(110);              // tell servo to go to position in variable 'pos'

}

void assemblyMove(){

  for (pos = 105; pos <= 115; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree

    int amplifiedPos = map(pos, 105, 115, 98, 122);
    Uplift.write(amplifiedPos);              // tell servo to go to position in variable 'pos'
    Recharge.write(amplifiedPos);              // tell servo to go to position in variable 'pos'

    Energise.write(pos);              // tell servo to go to position in variable 'pos'
    Invigorate.write(pos);              // tell servo to go to position in variable 'pos'

    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 105; pos >= 115; pos -= 1) { // goes from 180 degrees to 0 degrees

    int amplifiedPos = map(pos, 105, 115, 98, 122);
    Uplift.write(amplifiedPos);              // tell servo to go to position in variable 'pos'
    Recharge.write(amplifiedPos);              // tell servo to go to position in variable 'pos'

    Energise.write(pos);              // tell servo to go to position in variable 'pos'
    Invigorate.write(pos);              // tell servo to go to position in variable 'pos'

    delay(15);                       // waits 15ms for the servo to reach the position
  }


}

void speakerMove(Servo *servo){
  // vibrate a random one - he's the speaker

  for (pos = 105; pos <= 115; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    servo->write(pos);              // tell servo to go to position in variable 'pos'

    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (pos = 105; pos >= 115; pos -= 1) { // goes from 180 degrees to 0 degrees
    servo->write(pos);              // tell servo to go to position in variable 'pos'

    delay(15);                       // waits 15ms for the servo to reach the position
  }

}

void lookUplift(){

  Uplift.write(110);              // tell servo to go to position in variable 'pos'
  Recharge.write(155);              // tell servo to go to position in variable 'pos'
  Energise.write(155);              // tell servo to go to position in variable 'pos'
  Invigorate.write(155);              // tell servo to go to position in variable 'pos'

  delay(50);                       // waits 15ms for the servo to reach the position

}

void lookRecharge(){
  Uplift.write(55);              // tell servo to go to position in variable 'pos'
  Recharge.write(100);              // tell servo to go to position in variable 'pos'
  Energise.write(155);              // tell servo to go to position in variable 'pos'
  Invigorate.write(155);              // tell servo to go to position in variable 'pos'

  delay(50);                       // waits 15ms for the servo to reach the position

}

void lookEnergise(){
  Uplift.write(55);              // tell servo to go to position in variable 'pos'
  Recharge.write(55);              // tell servo to go to position in variable 'pos'
  Energise.write(100);              // tell servo to go to position in variable 'pos'
  Invigorate.write(55);              // tell servo to go to position in variable 'pos'

  delay(50);                       // waits 15ms for the servo to reach the position

}

void lookInvigorate(){
  Uplift.write(55);              // tell servo to go to position in variable 'pos'
  Recharge.write(55);              // tell servo to go to position in variable 'pos'
  Energise.write(155);              // tell servo to go to position in variable 'pos'
  Invigorate.write(100);              // tell servo to go to position in variable 'pos'

  delay(50);                       // waits 15ms for the servo to reach the position

}

void startState(State newState){
  Serial.print("start state ");
  Serial.println(newState);
  currentState = newState;
  looking = false;
  stateTimeStart = millis();
  assemblyMove();

  timeoutStart = millis();
}



// Cloud Functions
int idle(String extra) {
  currentState = IDLE;
  faceForward();
  return 0;
}

int startStateA(String extra) {
  startState(STATE_A);
  return 0;
}
int startStateB(String extra) {
  startState(STATE_B);
  return 0;
}
int startStateC(String extra) {
  startState(STATE_C);
  return 0;
}
int startStateD(String extra) {
  startState(STATE_C);
  return 0;
}
int startStateE(String extra) {
  startState(STATE_E);
  return 0;
}
int startStateF(String extra) {
  startState(STATE_F);
  return 0;
}
int startStateG(String extra) {
  startState(STATE_G);
  return 0;
}
int startStateH(String extra) {
  startState(STATE_H);
  return 0;
}


/*int selection(String extra) {
  Serial.println("selection");
  return 0;
}

int drop(String extra) {
  Serial.println("drop");
  return 0;
}*/
